1
00:00:00,500 --> 00:00:02,180
Welcome to the Tri-C
Center for Learning

2
00:00:02,180 --> 00:00:04,260
Excellence video
series on creating

3
00:00:04,260 --> 00:00:08,060
an online portfolio
with Weebly.

4
00:00:08,060 --> 00:00:10,130
In this last video,
we will go over a

5
00:00:10,130 --> 00:00:12,070
few additional
features and settings

6
00:00:12,070 --> 00:00:14,180
of the Weebly
editor.

7
00:00:14,180 --> 00:00:17,050
The first thing we
will look at are Apps.

8
00:00:17,050 --> 00:00:19,070
Clicking on the Apps
tab will take you

9
00:00:19,070 --> 00:00:20,140
to the App Center.

10
00:00:20,140 --> 00:00:22,050
This is where you
can add apps that

11
00:00:22,050 --> 00:00:23,250
will provide your
portfolio with

12
00:00:23,250 --> 00:00:26,200
additional functionality
and features.

13
00:00:26,200 --> 00:00:28,110
Just like the app
stores on your phone

14
00:00:28,110 --> 00:00:30,060
and computer, you
can search by

15
00:00:30,060 --> 00:00:32,140
category or keyword.

16
00:00:37,240 --> 00:00:39,270
Just as before, we
we will continue to

17
00:00:39,270 --> 00:00:42,050
be prompted to select
a domain each time

18
00:00:42,050 --> 00:00:45,060
we return to the
editor.

19
00:00:47,090 --> 00:00:49,090
In the Settings tab,
you can change your

20
00:00:49,090 --> 00:00:51,160
domain name, if
necessary.

21
00:00:51,160 --> 00:00:54,060
Since we have not
set ours up yet, there

22
00:00:54,060 --> 00:00:56,060
is a link for us to
do so.


23
00:00:56,060 --> 00:00:58,050
There is also the
option to change the

24
00:00:58,050 --> 00:01:00,070
title of your
portfolio, as well

25
00:01:00,070 --> 00:01:02,050
as it's category.

26
00:01:02,050 --> 00:01:04,060
There are also some
additional settings

27
00:01:04,060 --> 00:01:05,260
that you don't have
to worry about for

28
00:01:05,260 --> 00:01:08,020
setting up your
portfolio.

29
00:01:11,000 --> 00:01:13,140
In the SEO tab, you
will find features

30
00:01:13,140 --> 00:01:15,120
that help sites rank
better in search

31
00:01:15,120 --> 00:01:18,110
engines, like descriptions
and keyword tags.

32
00:01:18,110 --> 00:01:20:050
You can skip this
section for your

33
00:01:20,050 --> 00:01:22,120
portfolio, but this
is a good tab to

34
00:01:22,120 --> 00:01:24,100
remember if you want
to improve its ranking

35
00:01:24,100 --> 00:01:26,100
in web searches.

36
00:01:26,100 --> 00:01:28,200
You can also add editors
to your portfolio,

37
00:01:28,200 --> 00:01:31,020
if your professor requires
it or if you have

38
00:01:31,020 --> 00:01:34,090
a friend or coach that
is helping you build it.

39
00:01:34,090 --> 00:01:36,180
The members tab is not
a tab you'll use,

40
00:01:36,180 --> 00:01:39,190
since portfolios do
not offer memberships.

41
00:01:39,190 --> 00:01:42,090
The My Apps tab shows
the apps that are

42
00:01:42,090 --> 00:01:44,060
currently installed in
your portfolio

43
00:01:44,060 --> 00:01:46,200
and allows you to edit
their options.

44
00:01:46,200 --> 00:01:48,120
There is also a link
to go to the App

45
00:01:48,120 --> 00:01:50,100
Center from here to
add any additional

46
00:01:50,100 --> 00:01:52,130
ones you might need.

47
00:01:52,130 --> 00:01:54,220
Like a few of the
options, you don't

48
00:01:54,220 --> 00:01:56,180
have to worry about
the Blog tab for your

49
00:01:56,180 --> 00:01:58,230
portfolio, unless you
want to set one up to

50
00:01:58,230 --> 00:02:00,240
keep your visitors
up-to-date with

51
00:02:00,240 --> 00:02:03,150
current projects you
are working on and planning.

52
00:02:03,150 --> 00:02:06,120
If you set up a blog,
this is the tab that

53
00:02:06,120 --> 00:02:08,160
you would use to adjust
its settings.

54
00:02:08,160 --> 00:02:10,170
For now, though, the
only option is to

55
00:02:10,170 --> 00:02:12,290
click here to set
one up.

56
00:02:12,290 --> 00:02:15,060
You can use the Help
tab at the top of the

57
00:02:15,060 --> 00:02:17,130
page to try to get
assistance with anything

58
00:02:17,130 --> 00:02:19,060
that might not be
working or if your are

59
00:02:19,060 --> 00:02:21,020
having difficulty
figuring out how to

60
00:02:21,020 --> 00:02:23,110
do a particular task.

61
00:02:23,110 --> 00:02:25,220
If we go back to the
Editor, there is one

62
00:02:25,220 --> 00:02:28,160
last feature to look
at.

63
00:02:28,160 --> 00:02:31,070
This icon to the right
of Help allows you

64
00:02:31,070 --> 00:02:33,250
to change your preview
of your portfolio.

65
00:02:33,250 --> 00:02:36,050
You can toggle between
previewing it as if

66
00:02:36,050 --> 00:02:38,120
it was being viewed
on a deskop or on

67
00:02:38,120 --> 00:02:39,270
a mobile device.

68
00:02:39,270 --> 00:02:41,150
This gives you a
preview of what your

69
00:02:41,150 --> 00:02:43,070
portfolio would look
like if a visitor

70
00:02:43,070 --> 00:02:45,500
loaded it on their
phone.

71
00:02:49,190 --> 00:02:51,290
You can switch back
to desktop mode by

72
00:02:51,290 --> 00:02:54,130
clicking on the
same icon.

73
00:02:54,130 --> 00:02:56,240
Remember, the changes
that you make to your

74
00:02:56,240 --> 00:02:59,000
portfolio are not
saved until you click

75
00:02:59,000 --> 00:03:00,280
the publish button.

76
00:03:00,280 --> 00:03:03,000
You are now familiar
with all of the basics

77
00:03:03,000 --> 00:03:06,060
needed to prepare your
online portfolio with Weebly.
