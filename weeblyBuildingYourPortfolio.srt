1
00:00:00,000 --> 00:00:02,140
Welcome to the Tri-C
Center for Learning

2
00:00:02,140 --> 00:00:04,230
Excellence video
series on creating

3
00:00:04,230 --> 00:00:07:100
an online portfolio
with Weebly.

4
00:00:07,100 --> 00:00:09,200
In this video, we
will cover customizing

5
00:00:09,200 --> 00:00:12140
the theme and building
the portfolio.

6
00:00:12,140 --> 00:00:14,260
In Weebly's editor,
on the top row,

7
00:00:14,260 --> 00:00:16,260
clicking on the Theme
tab will bring you to

8
00:00:16,260 --> 00:00:18,280
the options that are
available to customize

9
00:00:18,280 --> 00:00:20,050
your theme.

10
00:00:20,050 --> 00:00:22,030
The options that are
here will vary,

11
00:00:22,030 --> 00:00:24,230
depending on your
chosen theme, but this

12
00:00:24,230 --> 00:00:26,120
is where you will go
to change your theme

13
00:00:26,120 --> 00:00:29,280
and the fonts that
your theme uses.

14
00:00:29,280 --> 00:00:32,110
Clicking on the Pages
tab will allow you to

15
00:00:32,110 --> 00:00:34,080
change the order of
the links that are in

16
00:00:34,080 --> 00:00:36,250
your navigation bar
by dragging and dropping

17
00:00:36,250 --> 00:00:39,020
the page names into a
different order in the

18
00:00:39,020 --> 00:00:41,260
list to the side.

19
00:00:41,260 --> 00:00:43,260
Clicking on one of the
page names will bring

20
00:00:43,260 --> 00:00:45,180
you to the page and
present you with some

21
00:00:45,180 --> 00:00:48,040
options to change the
page name and adjust

22
00:00:48,040 --> 00:00:51,020
some options for the
page's header.

23
00:00:56,110 --> 00:00:59,010
You can also navigate
to any page by clicking

24
00:00:59,010 --> 00:01:01,130
on the links in your
navigation bar just

25
00:01:01,130 --> 00:01:03,170
as your visitors will.

26
00:01:03,170 --> 00:01:06,000
To edit the contents
of your selected page,

27
00:01:06,000 --> 00:01:08,160
you will click on the
Build tab.

28
00:01:08,160 --> 00:01:11,080
The Build tab allows
you to edit items that

29
00:01:11,080 --> 00:01:13,140
are already present
in your portfolio,

30
00:01:13,140 --> 00:01:16,170
add new items from those
available on the left,

31
00:01:16,170 --> 00:01:18,290
or delete items by
clicking on the X that

32
00:01:18,290 --> 00:01:21,240
is on the top-right
corner of their box.

33
00:01:21,240 --> 00:01:24,100
To edit an item, all
you have to do is

34
00:01:24,100 --> 00:01:25,110
click on it.

35
00:01:25,110 --> 00:01:27,290
For text, that allows
you to edit it just as

36
00:01:27,290 --> 00:01:30,090
if it was in a text
document; for other

37
00:01:30,090 --> 00:01:32,160
other items, you will
be presented with a

38
00:01:32,160 --> 00:01:34,220
pop-up window that will
allow you to adjust

39
00:01:34,220 --> 00:01:36,100
its details.

40
00:01:36,100 --> 00:01:38,140
Once you are done
editing an item,

41
00:01:38,140 --> 00:01:42,020
click outside of its
box to exit editing mode.

42
00:01:42,020 --> 00:01:44,280
To add an item, click
on the item that you

43
00:01:44,280 --> 00:01:46,280
want from the left,
and drag and drop it

44
00:01:46,280 --> 00:01:49,120
to where you would
like it to be.

45
00:01:50,170 --> 00:01:53,260
So, let's add an image to
the top of this left column.

46
00:01:53,260 --> 00:01:56,150
We'll click on the
image icon, drag it

47
00:01:56,150 --> 00:01:58,250
to the top of the
column, and then click

48
00:01:58,250 --> 00:02:00,260
it to edit what
it shows.

49
00:02:00,260 --> 00:02:02,260
I'll select to upload
a picture from my

50
00:02:02,260 --> 00:02:05,050
computer, and then
select an image from

51
00:02:05,050 --> 00:02:08,040
the window that opens
up off-screen.

52
00:02:08,040 --> 00:02:10,140
Another item you might
want to include would

53
00:02:08,040 --> 00:02:12,280
be a document.

54
00:02:12,280 --> 00:02:15,090
To add a document, you
need to click on the

55
00:02:15,090 --> 00:02:17,140
Show All Elements link
at the bottom of the
 
56
00:02:17,140 --> 00:02:20,030
icons to the left,
scroll down to where

57
00:02:20,030 --> 00:02:22,280
it says file, and drag
the file icon to where

58
00:02:22,280 --> 00:02:25,110
you want it.

59
00:02:27,150 --> 00:02:30,500
Let's split this
column into two.

60
00:02:32,290 --> 00:02:35,280
Next, I'll click to
edit it, choose upload

61
00:02:35,280 --> 00:02:39,030
again, and then select
a PDF document from

62
00:02:39,030 --> 00:02:41,210
the window that
appears off-screen.

63
00:02:43,150 --> 00:02:45,110
This will present an
icon that your

64
00:02:45,110 --> 00:02:47,120
visitors can click
on to download

65
00:02:47,120 --> 00:02:49,200
the document.

66
00:02:49,200 --> 00:02:52,130
Notice that the theme
automatically adjusts

67
00:02:52,130 --> 00:02:55,110
the positioning of
the items you add.

68
00:02:55,110 --> 00:02:57:220
One last item that you
might want to add

69
00:02:55,110 --> 00:02:59,210
would be a
YouTube video.

70
00:02:59,210 --> 00:03:01,270
This could be a video
introduction for your

71
00:03:01,270 --> 00:03:03,230
visitors or a
presentation that

72
00:03:03,230 --> 00:03:05,110
you would like
them to see.

73
00:03:05,110 --> 00:03:07,270
To add this, you will
drag-and-drop the

74
00:03:07,270 --> 00:03:10,140
YouTube icon from the
options on the left to

75
00:03:10,140 --> 00:03:12,100
where you would like
it.

76
00:03:12,100 --> 00:03:15,000
Then, click on it and
type the URL of the

77
00:03:15,000 --> 00:03:18,110
video and click outside
of the pop-up window to

78
00:03:18,110 --> 00:03:21,030
have Weebly load it into
your portfolio.

79
00:03:21,030 --> 00:03:23,200
Make sure that the
video is public or

80
00:03:23,200 --> 00:03:25,240
else it won't be
accessible.

81
00:03:25,240 --> 00:03:28,270
Now, visitors will
be able to view the

82
00:03:28,270 --> 00:03:30,150
video inside your
portfolio instead

83
00:03:30,150 --> 00:03:33:03
of leaving to go
to YouTube.

84
00:03:36,130 --> 00:03:38,220
To remove an item,
all you'll have to

85
00:03:38,220 --> 00:03:42,060
do is click the X in
its top-right corner.

86
00:03:42,060 --> 00:03:44,050
The theme will
automatically adjust

87
00:03:44,050 --> 00:03:46,130
the spacing of the
items around it to

88
00:03:46,130 --> 00:03:49,170
ensure that no
unwanted gaps remain.

89
00:03:49,170 --> 00:03:52,030
You can also move items
on the page by

90
00:03:52,030 --> 00:03:53,240
dragging and dropping
them from where

91
00:03:53,240 --> 00:03:56,180
they are to a
new location.

92
00:03:56,180 --> 00:03:58,230
After you are done
making any changes

93
00:03:58,230 --> 00:04:00,230
you would like to
your portfolio,

94
00:04:00,230 --> 00:04:03,090
be sure to click on
the Publish button.

95
00:04:03,090 --> 00:04:05,000
Every time you make
a change to your

96
00:04:05,000 --> 00:04:07,130
portfolio or update
it, the changes are

97
00:04:07,130 --> 00:04:10,090
not saved until the new
version is published.

98
00:04:10,090 --> 00:04:12,140
Now for us, we would
not be able to

99
00:04:12,140 --> 00:04:14,100
publish this because
we have not chosen a

100
00:04:14,100 --> 00:04:16,160
domain; you have to
choose a domain

101
00:04:16,160 --> 00:04:18,230
before any changes
can be saved.
