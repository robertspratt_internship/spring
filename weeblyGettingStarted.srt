1
00:00:00,000 --> 00:00:02,170
Welcome to the Tri-C
Center for Learning

2
00:00:02,170 --> 00:00:04,220
Excellence video
series on creating

3
00:00:04,220 --> 00:00:07,160
an online portfolio
with Weebly. In this

4
00:00:07,160 --> 00:00:09,170
video we will cover
signing up for an

5
00:00:09,170 --> 00:00:11,210
account and all of
the necessary steps

6
00:00:11,210 --> 00:00:13,040
to get started.

7
00:00:13,040 -->00:00:14,500
Once you have made
it to Weebly's

8
00:00:14,500 --> 00:00:17,030
website, the first
thing you'll need to

9
00:00:17,030 --> 00:00:18,160
do is click on the
click on the Sign Up

10
00:00:18,160 --> 00:00:21,050
button in the top
right corner.

11
00:00:21,050 --> 00:00:23,040
This will take you to
a form to fill out to

12
00:00:23,040 --> 00:00:24,280
register for an account.

13
00:00:24,280 --> 00:00:26,170
After you have filled
out all of the

14
00:00:26,170 --> 00:00:28,250
required fields and
marked the required

15
00:00:28,250 --> 00:00:31,210
boxes, go ahead and
click continue to go

16
00:00:31,210 --> 00:00:33,130
to the next page.

17
00:00:34,200 --> 00:00:36,130
Next, Weebly will ask
what kind of

18
00:00:36,136 --> 00:00:38,060
website you want to
create.

19
00:00:38,060 --> 00:00:40,010
Since you will be
using this for your

20
00:00:40,010 --> 00:00:43,000
personal portfolio,
choose Try Weebly

21
00:00:43,000 --> 00:00:45,030
Websites on the
right.

22
00:00:45,030 --> 00:00:47,027
The next step is to
select a theme.

23
00:00:47,027 --> 00:00:50,000
You can both change
and customize the

24
00:00:50,000 --> 00:00:52,060
theme you choose
later, so do not

25
00:00:52,060 --> 00:00:54,020
worry too much about
finding the perfect

24
00:00:54,020 --> 00:00:56,160
one; this is just
the template for

25
00:00:56,160 --> 00:00:58,240
start your
portfolio with.

26
00:00:58,240 --> 00:01:00,230
Weebly has sorted
its themes into

27
00:01:00,230 --> 00:01:02,290
different categories,
but you do not have

28
00:01:02,290 --> 00:01:05,260
choose a theme from
the portfolio category.

29
00:01:05,260 --> 00:01:07,160
When you find a
theme that your are

30
00:01:07,160 --> 00:01:09,210
interested in, you
can click on it to

31
00:01:09,210 --> 00:01:12,270
see a sample website
built with that theme.

32
00:01:12,270 --> 00:01:15,110
Once you find theme
you like, click the

33
00:01:15,110 --> 00:01:19,030
Start Editing button
in the top right corner.

34
00:01:19,030 --> 00:01:21,230
Weebly will then load
you into the editor

35
00:01:21,230 --> 00:01:23,220
to start building
your portfolio.

36
00:01:23,220 --> 00:01:25,240

The first thing it
will ask you is to

37
00:01:25,240 --> 00:01:27,210
choose a domain name.

38
00:01:27,210 --> 00:01:29,180
This will be the web
address that users

39
00:01:29,180 --> 00:01:32,100
will type in to find
your portfolio.

40
00:01:32,100 --> 00:01:34,130
If you have already
purchased a domain

41
00:01:34,130 --> 00:01:36,270
name elsewhere,
you can choose to

42
00:01:36,270 --> 00:01:39:080
connect or transfer
it now.

43
00:01:39,080 --> 00:01:42,010
Alternatively, you
can use Weebly to

44
00:01:42,010 --> 00:01:43,250
search for a domain
name, or use a

45
00:01:43,250 --> 00:01:45,160
subdomain of Weebly
such as

46
00:01:45,160 --> 00:01:49,030
yourname.weebly.com,
to get one for free.

47
00:01:49,030 --> 00:01:53,210
You can also choose the
X to decide this later.

48
00:01:53,210 --> 00:01:56,080
You will then be
notified of any apps

49
00:01:56,080 --> 00:01:58,030
that the theme comes
with that need to

50
00:01:58,030 --> 00:01:59,080
be updated.

51
00:01:59,080 --> 00:02:01,270
If any apps were updated,
like the color block

52
00:02:01,270 --> 00:02:04,090
app here, you will
need to reload the

53
00:02:04,090 --> 00:02:07,130
editor for the updates
to be applied.

54
00:02:08,250 --> 00:02:11,130
Every time you load
the editor, it will

55
00:02:11,130 --> 00:02:13,230
ask you to select a
domain name until you

56
00:02:13,230 --> 00:02:15,020
have done so.

57
00:02:15,020 --> 00:02:17,050
Since we are skipping
this for now, I will

58
00:02:17,050 --> 00:02:20,160
go ahead and click
the X again.

59
00:02:20,160 --> 00:02:22,240
Your portfolio is
now set up and

60
00:02:22,240 --> 00:02:25,190
ready for you to start
customizing and building.
